import recommendation.movies.Movie;

/**
 * The MovieTest class is used to test the Movie class
 * 
 * @author Joshua Bruder-Wexler
 * @date Sept. 20, 2018
 * @version 1.0
 *
 */
public class MovieTest {
	/**
	 * The whole method tests methods from the movie class based on the returns and whether or not they match what is expected
	 */
	public static void main (String[]args){
		Movie film = new Movie( "11,American President%2C The (1985),Comedy|Drama|Romance");
		if(film.getName().equals("American President, The")) {
			System.out.println("getName(): PASS");
		}
		else {
			System.out.println("getName(): FAIL");
		}
	if(film.getYear().equals("1985")) {
		System.out.println("getYear(): PASS");
	}
	else {
		System.out.println("getYear(): FAIL");
	}
	if(film.getId() == (11)){
		System.out.println("getId(): PASS");
	}
	else {
		System.out.println("getId(): FAIL");
	}
	if(film.getGenres()[1].equals("drama")) {
		System.out.println("getGenres: PASS");
	}
	else {
		System.out.println("getGenres(): FAIL");
		System.out.println(film.getGenres()[0]);
	}
	if(film.hasGenre("comedy")) {
		System.out.println("hasGenre(): PASS");
	}
	else {
		System.out.println("hasGenre(): FAIL");
	}
	}
}


