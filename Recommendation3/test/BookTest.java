import recommendation.book.Book;
import java.util.Arrays;

/**
 * The BookTest class is used to test the PopularRecommender and
 * PopularMovieRecommender classes
 * 
 * @author Liam Harbec
 * @author Joshua Bruder-Wexler
 * @date Nov. 13, 2018
 * @version 1.0
 *
 */
public class BookTest {
	public static void main(String[] args) {
		Book[] book = { new Book(
				"1,2767052,2767052,2792775,272,439023483,9.78E+12	,Suzanne Collins,2008,The Hunger Games,The Hunger Games (The Hunger Games%2C #1),eng,4.34,4780653,4942365,155254,66715,127936,560092,1481305,2706317,https://images.gr-assets.com/books/1447303603m/2767052.jpg,https://images.gr-assets.com/books/1447303603s/2767052.jpg"),
				new Book(
						"2,3,3,4640799,491,439554934,9.78E+12,J.K. Rowling,1997,Harry Potter and the Philosopher's Stone,Harry Potter and the Sorcerer's Stone (Harry Potter%2C #1),eng,4.44,4602479,4800065,75867,75504,101676,455024,1156318,3011543,https://images.gr-assets.com/books/1474154022m/3.jpg,https://images.gr-assets.com/books/1474154022s/3.jpg"),
				new Book(
						"3,41865,41865,3212258,226,316015849,9.78E+12,Stephenie Meyer,2005,Twilight,Twilight (Twilight%2C #1),en-US,3.57,3866839,3916824,95009,456191,436802,793319,875073,1355439,https://images.gr-assets.com/books/1361039443m/41865.jpg,https://images.gr-assets.com/books/1361039443s/41865.jpg") };

		// Test for book 1
		System.out.println("Book #1");
		if (book[0].getId() == 1) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
		if (book[0].getName().equals("The Hunger Games")) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
		if (Arrays.toString(book[0].getAuthors()).substring(1, Arrays.toString(book[0].getAuthors()).length() - 1)
				.equals("Suzanne Collins")) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
		if (book[0].getIsbn().equals("439023483")) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}

		// Test for book 2
		System.out.println("\nBook #2");
		if (book[1].getId() == 2) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
		if (book[1].getName().equals("Harry Potter and the Philosopher's Stone")) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
		if (Arrays.toString(book[1].getAuthors()).substring(1, Arrays.toString(book[1].getAuthors()).length() - 1)
				.equals("J.K. Rowling")) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
		if (book[1].getIsbn().equals("439554934")) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}

		// Test for book 3
		System.out.println("\nBook #3");
		if (book[2].getId() == 3) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
		if (book[2].getName().equals("Twilight")) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
		if (Arrays.toString(book[2].getAuthors()).substring(1, Arrays.toString(book[2].getAuthors()).length() - 1)
				.equals("Stephenie Meyer")) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
		if (book[2].getIsbn().equals("316015849")) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}

	}
}
