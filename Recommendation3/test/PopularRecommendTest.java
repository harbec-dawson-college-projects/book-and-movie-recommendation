import java.util.ArrayList;

import recommendation.movies.Movie;
import recommendation.movies.PopularMovieRecommender;
import recommendation.shared.PopularRecommender;
import recommendation.shared.Rating;

/**
 * The PopularRecommendTest class is used to test the PopularRecommender and
 * PopularMovieRecommender classes
 * 
 * @author Liam Harbec
 * @date Nov. 13, 2018
 * @version 1.0
 *
 */
public class PopularRecommendTest {
	/*
	 * The main method of the PopularRecommendTest class, used to create a Rating[],
	 * Movie[], a PopularRecommender object, and run several tests on both recommend
	 * methods
	 * 
	 * @date Sept. 24, 2018
	 */
	public static void main(String[] args) {
		// Each rating
		/*
		 * userId,movieId,rating,timestamp 1,31,2.5,1260759144
		 */
		Rating a = new Rating("1,24,3.6,647835738");
		Rating b = new Rating("1,1842,1.0");
		Rating c = new Rating("1,742,4.5");
		Rating d = new Rating("2,742,2.6");
		Rating e = new Rating("2,48364,4.9");
		Rating f = new Rating("3,1526,2.3");
		Rating g = new Rating("3,251,4.9");
		Rating h = new Rating("3,84674,2.0");
		Rating i = new Rating("3,846874,3.9");
		Rating j = new Rating("3,1842,0.2");
		// User 4 has not rating anything
		Rating k = new Rating("5,48364,3.7");
		Rating l = new Rating("6,251,4.2");
		Rating m = new Rating("6,24,4.5");
		Rating n = new Rating("7,84674,4.4");
		Rating o = new Rating("7,24,4.1");
		Rating p = new Rating("7,251,4.5");
		Rating q = new Rating("7,846874,3.9");
		Rating r = new Rating("8,1526,2.7");
		Rating s = new Rating("8,1842,2.0");
		// Creates Rating array
		ArrayList<Rating> ratings = new ArrayList<Rating>();
		ratings.add(a);
		ratings.add(b);
		ratings.add(c);
		ratings.add(d);
		ratings.add(e);
		ratings.add(f);
		ratings.add(g);
		ratings.add(h);
		ratings.add(i);
		ratings.add(j);
		ratings.add(k);
		ratings.add(l);
		ratings.add(m);
		ratings.add(n);
		ratings.add(o);
		ratings.add(p);
		ratings.add(q);
		ratings.add(r);
		ratings.add(s);
		
		// Each movie
		/*
		 * private String year; private String id; private String [] genres; private
		 * String name;
		 */

		Movie t = new Movie("24,Banana Boat Float (1997),Action|Comedy|Sci-Fi"); // Average is 4.066~
		Movie u = new Movie("742,Mickey Mouse Kills Daffy (2003),Drama|Fantasy|Murder"); // Average is 3.55
		Movie v = new Movie("1842,Police Crime Movie Thing ooo (2013),Crime|Drama|Murder|Police"); // Average is 1.066~
		Movie w = new Movie("251,Zombie Love (1326),Drama|Horror|Romance|Zombie"); // Average is 4.533~
		Movie x = new Movie("1526,A Bad Love Story (1973),Drama|Love|Romance"); // Average is 2.5
		Movie y = new Movie("84674,Lots of Superheroes (3018),Action|Death|Superhero"); // Average is 3.2
		Movie z = new Movie("48364,Your Typical Chick-Flick (2009),Action|Comedy|Romance"); // Average is 4.3
		Movie aA = new Movie("846874,Magician Killers (2010),Comedy|Crime|Magic|Murder"); // Average is 3.9
		// Creates Movie array
		ArrayList<Movie> list = new ArrayList<Movie>();
		list.add(t);
		list.add(u);
		list.add(v);
		list.add(w);
		list.add(x);
		list.add(y);
		list.add(z);
		list.add(aA);
		// Makes PopularRecommender object
		PopularRecommender<Movie> pr = new PopularRecommender<Movie>(list, ratings);
		System.out.println(pr.getItems().get(0));
		System.out.println(pr.getItems().get(1));
		int size = pr.getItems().size() - 1;
		System.out.println(pr.getItems().get(size));

		// Testing by top recommended by comparing Id's of returned with hard-coded
		// expected value
		System.out.println("\nNow testing with the top rated:\n");
		ArrayList<Movie> numTestA = pr.recommend(7, 5);

		if (numTestA.get(0).getName().equals("Your Typical Chick-Flick")) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
		ArrayList<Movie> numTestB = pr.recommend(7, 3);
		if (numTestB.get(0).getName().equals("Your Typical Chick-Flick")) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
		ArrayList<Movie> numTestC = pr.recommend(3, 4);
		if (numTestC.get(0).getName().equals("Your Typical Chick-Flick")) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
		ArrayList<Movie> numTestD = pr.recommend(4, 6);
		if (numTestD.get(0).getName().equals("Zombie Love")) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
		ArrayList<Movie> numTestE = pr.recommend(5, 9);
		if (numTestE.get(0).getName().equals("Zombie Love")) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}

		PopularMovieRecommender pmr = new PopularMovieRecommender(list, ratings);
		System.out.println("\n\nNow testing with the recommend genres:\n");

		// Testing with a genre by matching position 0 (best rated of numTest)
		ArrayList<Movie> numTestF = pmr.recommend(1, 10, "Action");
		if (numTestF.get(0).getName().equals("Your Typical Chick-Flick")) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
		ArrayList<Movie> numTestG = pmr.recommend(8, 10, "Drama");
		if (numTestG.get(0).getName().equals("Zombie Love")) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
		ArrayList<Movie> numTestH = pmr.recommend(3, 10, "Romance");
		if (numTestH.get(0).getName().equals("Your Typical Chick-Flick")) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
		ArrayList<Movie> numTestI = pmr.recommend(4, 10, "Zombie");
		if (numTestI.get(0).getName().equals("Zombie Love")) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
		ArrayList<Movie> numTestJ = pmr.recommend(7, 10, "Comedy");
		if (numTestJ.get(0).getName().equals("Your Typical Chick-Flick")) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}

	}
}