/**
bad  * This class reads calls on recommend methods and on the IMovieLensFileReader class in order
 * to read the files and store an array of movies and ratings
 * It gives recommendations to the user based off of user id and genre (the number of recommendations given is set to 10)
 * @author Liam Harbec
 * @author Joshua Bruder-Wexler
 * @version 1.0
 * @since November 13th 2018
 */

/*import all necessary classes to do this*/
import java.util.Scanner;
import java.io.*;

import recommendation.book.Book;
import recommendation.fileio.*;
import recommendation.interfaces.IMovieRecommender;
import recommendation.interfaces.IRecommender;
import recommendation.movies.Movie;
import recommendation.movies.PersonalizedMovieRecommender;
import recommendation.movies.PopularMovieRecommender;
import recommendation.shared.PersonalizedRecommender;
import recommendation.shared.PopularRecommender;
import recommendation.shared.Rating;

import java.util.ArrayList;

/**
 * This is the main application to be used as a UI to provide the user with
 * various recommendations based on specified criteria
 * 
 * @author Liam Harbec
 * @author Adrian Yotov
 * @version 1.0
 * @since Nov. 13, 2018
 */
public class RecommenderApplication {

	/**
	 * The main method
	 * 
	 * @no return (returns void)
	 */
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.println("Hello there!");
		System.out.println("Would you like recommendations for Movies or Books?");
		String itemType = reader.nextLine().toLowerCase();
		String itemPath = null;
		String ratingsPath = null;
		ArrayList<Movie> movieList = null;
		ArrayList<Book> bookList = null;
		ArrayList<Rating> ratings = null;

		// Ensures that they enter either movie or book
		while (!(itemType.equals("movie") || itemType.equals("book"))) {
			System.out.println("Invalid type, please enter either a Movie or Book!");
			itemType = reader.nextLine().toLowerCase();
		}

		// Will execute if the type is a Movie
		if (itemType.equals("movie")) {
			// This will be used on the fileio method "loadMovies"
			System.out.println("Enter a path for a movies file");
			itemPath = reader.nextLine();
			// This will be used on the fileio method "loadRatings"
			System.out.println("Enter a path for a movie ratings file");
			ratingsPath = reader.nextLine();

			try {
				movieList = MovieLensFileReader.loadMovies(itemPath);
			} catch (IOException e) {
				reader.close();
				throw new IllegalArgumentException("Please enter a valid path for a movies file");
			}
		}

		// Will execute if the type is a Book
		else if (itemType.equals("book")) // An else if for future Object creations
		{
			// This will be used on the fileio method "loadBooks"
			System.out.println("Enter a path for a books file");
			itemPath = reader.nextLine();
			// This will be used on the fileio method "loadRatings"
			System.out.println("Enter a path for a book ratings file");
			ratingsPath = reader.nextLine();

			try {
				bookList = GoodReadsFileReader.loadBooks(itemPath);
			} catch (IOException e) {
				reader.close();
				throw new IllegalArgumentException("Please enter a valid path for a books file");
			}
		}

		// Loads ratings to a ArrayList<Rating>, respective to which Item type it is
		try {
			ratings = ItemFileReader.loadRatings(ratingsPath);
		} catch (IOException e) {
			reader.close();
			throw new IllegalArgumentException("Please enter a valid " + itemType + " ratings path");
		}

		System.out.println("What kind of recommendation were you looking for? (popular or personalized)");
		String type = reader.nextLine().toLowerCase();

		// The variables below are initialized so their values can exist outside of the
		// if statements below
		int userid = 0;
		String genre = null;

		// Ensures that they enter either popular or personalized
		while (!(type.equals("popular") || type.equals("personalized"))) {
			System.out.println("Invalid recommendation type, please enter either popular or personalized!");
			type = reader.nextLine().toLowerCase();
		}

		// If the item type is a movie
		if (itemType.equals("movie")) {
			while (userid >= 0) {

				// Prompting user for a genre and a userid
				System.out.println("Please enter the genre you would like to view");
				System.out.println("Enter \"all\" if you'd like to view all genres");
				genre = reader.nextLine().toLowerCase(); // Can't do error checking as there are many different
				// genres

				IMovieRecommender recommended = null;
				if (type.toLowerCase().equals("popular")) {
					recommended = new PopularMovieRecommender(movieList, ratings);

					System.out.println("Please enter your userId");
					userid = reader.nextInt();
					if (genre.equals("all")) {
						// The variable will store 10 as this is a test to ensure that it works
						print(recommended.recommend(userid, 10));
					} else {
						print(recommended.recommend(userid, 10, genre));
					}
				}
				if (type.toLowerCase().equals("personalized")) {
					recommended = new PersonalizedMovieRecommender(movieList, ratings);

					// Prompting user for a genre and a userid
					System.out.println("Please enter your userId");
					userid = reader.nextInt();
					if (genre.equals("all")) {
						// The variable will store 10 as this is a test to ensure that it works
						print(recommended.recommend(userid, 10));
					} else {
						print(recommended.recommend(userid, 10, genre));
					}
				}
				
				System.out.println("If you would like more recommendations, please enter a positive value");
				System.out.println("If you would like to close the program, enter a negative value");
				userid = reader.nextInt();
				reader.nextLine();
			}
		}

		// If the item type is a book
		if (itemType.equals("book")) {

			while (userid >= 0) {

				IRecommender recommender = null;

				if (type.toLowerCase().equals("popular")) {
					recommender = new PopularRecommender(bookList, ratings);

					System.out.println("Please enter your userId");
					userid = reader.nextInt();

					// The variable will store 10 as this is a test to ensure that it works
					print(recommender.recommend(userid, 10));

				}
				if (type.toLowerCase().equals("personalized")) {
					recommender = new PersonalizedRecommender(bookList, ratings);

					System.out.println("What is your user Id?");
					userid = reader.nextInt();
					// The variable will store 10 as this is a test to ensure that it works

					print(recommender.recommend(userid, 10));
				}
				System.out.println("If you would like more recommendations, please enter a positive value");
				System.out.println("If you would like to close the program, enter a negative value");
				userid = reader.nextInt();
				reader.nextLine();
			}
			reader.close();
		}
	}

	/**
	 * This method will be used to print the contents of an ArrayList<T>
	 * 
	 * @param array
	 *            An ArrayList<T> to be printed
	 */
	public static <T> void print(ArrayList<T> array) {
		if (array.isEmpty()) {
			System.out.println("There are no recommendations for this user!");
		} else {
			for (T i : array) {
				System.out.println(i);
			}
		}
	}
}
