import recommendation.shared.Rating;

/**
 * The RatingTest class is used to test the initialization of a Rating type
 * 
 * @author Adrian Yotov
 * @author Liam Harbec
 * @date Nov. 13, 2018
 * @version 1.0
 *
 */
public class RatingTest {

	/**
	 * Main method to test the Rating class
	 * 
	 * @param x
	 *            is a Rating object for testing
	 */
	public static void main(String[] args) {
		Rating x = new Rating("1,3671,3");
		if (x.getUserId() == 1) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
		if (x.getId() == 3671) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
		if (x.getRating() == 3) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");

		}

	}

}
