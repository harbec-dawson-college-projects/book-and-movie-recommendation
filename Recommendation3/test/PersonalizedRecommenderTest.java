import recommendation.movies.Movie;
import recommendation.shared.PersonalizedRecommender;
import recommendation.shared.Rating;

import java.util.ArrayList;

/**
 * The PersonalizedRecommenderTest class is used to test the
 * PersonalizedRecommender class
 * 
 * @author Joshua Bruder-Wexler
 * @date Nov. 13, 2018
 * @version 1.0
 *
 */
public class PersonalizedRecommenderTest {

	public static void main(String[] args) {
		Rating a = new Rating("1,24,3.6,647835738");
		Rating b = new Rating("1,1842,1.0");
		Rating c = new Rating("1,742,4.5");
		Rating d = new Rating("2,742,2.6");
		Rating e = new Rating("2,48364,4.9");
		Rating f = new Rating("3,1526,2.3");
		Rating g = new Rating("3,251,4.9");
		Rating h = new Rating("3,84674,2.0");
		Rating i = new Rating("3,846874,3.9");
		Rating j = new Rating("3,1842,0.2");
		
		ArrayList<Rating> ratings = new ArrayList<Rating>();
		ratings.add(a);
		ratings.add(b);
		ratings.add(c);
		ratings.add(d);
		ratings.add(e);
		ratings.add(f);
		ratings.add(g);
		ratings.add(h);
		ratings.add(i);
		ratings.add(j);
		
		Movie t = new Movie("24,Banana Boat Float (1997),Action|Comedy|Sci-Fi"); // Average is 4.066~
		Movie u = new Movie("742,Mickey Mouse Kills Daffy (2003),Drama|Fantasy|Murder"); // Average is 3.55
		Movie v = new Movie("1842,Police Crime Movie Thing ooo (2013),Crime|Drama|Murder|Police"); // Average is 1.066~
		Movie w = new Movie("251,Zombie Love (1326),Drama|Horror|Romance|Zombie"); // Average is 4.533~
		Movie x = new Movie("1526,A Bad Love Story (1973),Drama|Love|Romance"); // Average is 2.5
		Movie y = new Movie("84674,Lots of Superheroes (3018),Action|Death|Superhero"); // Average is 3.2
		Movie z = new Movie("48364,Your Typical Chick-Flick (2009),Action|Comedy|Romance"); // Average is 4.3
		Movie aA = new Movie("846874,Magician Killers (2010),Comedy|Crime|Magic|Murder"); // Average is 3.9
		ArrayList<Movie> list = new ArrayList<Movie>();
		list.add(t);
		list.add(u);
		list.add(v);
		list.add(w);
		list.add(x);
		list.add(y);
		list.add(z);
		list.add(aA);
	/*	int users = PersonalizedRecommender.userCheck(ratings);

		if (users == 3) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
*/
	}

}
