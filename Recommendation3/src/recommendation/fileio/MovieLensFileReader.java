package recommendation.fileio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import recommendation.movies.Movie;

/**
 * The MovieLensFileReader is used to convert a String that contains the attributes to a Movie into a Movie
 * 
 * @author Liam Harbec
 * @version 1.0
 * @since Nov. 13, 2018
 */
public class MovieLensFileReader {

	// path i.e. /Recommendation3/datafiles/sorted/movies.csv
	public static ArrayList<Movie> loadMovies(String path) throws IOException {
		// Gets the Path variable
		Path p = Paths.get(path);
		// Creates a List of all lines in the file
		List<String> lines = Files.readAllLines(p);
		lines.remove(0);
		
		ArrayList<Movie> movies = new ArrayList<Movie>();
		for (String s : lines) {
			if (s.equals(",,,,"))
			{
				continue;
			}
			movies.add(convertToMovie(s));
		}
		
		return movies;
	}

	/**
	 * This method is used to convert a String to a Movie
	 * 
	 * @param movie A String that contains the attributes to a Movie
	 * @return m A Movie that has been initialized from a String
	 */
	public static Movie convertToMovie(String movie) {
		Movie m = new Movie(movie);
		return m;
	}

}
