package recommendation.fileio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import recommendation.shared.Rating;

/**
 * The ItemFileReader class is used to initialize a Ratings[] from a file that
 * contains the attributes for a Rating
 * 
 * @author Liam Harbec
 * @version 1.0
 * @since Nov. 13, 2018
 */
public class ItemFileReader {

	// path i.e. /Recommendation3/datafiles/sorted/movie_ratings.csv
	public static ArrayList<Rating> loadRatings(String path) throws IOException {
		// Gets the Path variable
		Path p = Paths.get(path);
		// Creates a List of all lines in the file
		List<String> lines = Files.readAllLines(p);
		lines.remove(0);

		// Makes a Rating[] based on the movie entries in the lines variable
		ArrayList<Rating> ratings = new ArrayList<Rating>();
		for (String s : lines) {
			ratings.add(convertToRating(s));
		}
		return ratings;
	}

	/**
	 * This method is used to convert a String to a Rating type
	 * 
	 * @param rating A String that contains the information to one Rating
	 * @return r A Rating to be returned
	 */
	public static Rating convertToRating(String rating) {
		Rating r = new Rating(rating);
		return r;
	}

}
