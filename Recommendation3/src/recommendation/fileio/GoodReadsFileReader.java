package recommendation.fileio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import recommendation.book.Book;

/**
 * The GoodReadsFileReader class is used to load a table from a file to a usable String
 * 
 * @author Liam Harbec
 * @author Joshua Bruder-Wexler
 * @version 1.0
 * @since Nov. 13, 2018
 */
public class GoodReadsFileReader {

	// path i.e. /Recommendation3/datafiles/sorted/books.csv
	public static ArrayList<Book> loadBooks(String path) throws IOException {
		// Gets the Path variable
		Path p = Paths.get(path);
		// Creates a List of all lines in the file
		List<String> lines = Files.readAllLines(p);
		lines.remove(0);

		ArrayList<Book> books = new ArrayList<Book>();
		for (String s : lines) {
			books.add(convertToBook(s));
		}
		return books;
	}

	/**
	 * This method converts a String to a Book type
	 * 
	 * @param book A String that contains the information or a single Book
	 * @return m A Book that has been initialized
	 */
	public static Book convertToBook(String book) {
		Book m = new Book(book);
		return m;
	}

}
