package recommendation.movies;

import java.util.ArrayList;
import java.util.HashMap;

import recommendation.interfaces.IMovieRecommender;
import recommendation.shared.PersonalizedRecommender;
import recommendation.shared.Rating;

public class PersonalizedMovieRecommender extends PersonalizedRecommender<Movie> implements IMovieRecommender
{
	private Movie[] movies;
	private HashMap<Integer, Integer> mostSimilarUsers;
	private HashMap<Integer, ArrayList<Rating>> usersRatings;

	public PersonalizedMovieRecommender(ArrayList<Movie> movies, ArrayList<Rating> ratings)
	{
		super(movies, ratings);
		
		this.mostSimilarUsers = super.getMostSimilarUsers();
		this.usersRatings = super.getUsersRatings();
	}
	
	
	/**
	 * recommend is a method also used to recommend movies the user has not rated
	 * yet from a certain genre according to the most similar user's ratings
	 * 
	 * @param userId
	 *            The userId to search for
	 * @param genre
	 *            The genre wanted
	 * @param numWanted
	 *            The number of movies needed
	 * @return movie[] The list of movies corresponding to parameters
	 */
	public ArrayList<Movie> recommend(int userId, int numWanted, String genre) {
		int similarId = this.mostSimilarUsers.get(userId);
		if (similarId != -1) {
			ArrayList<Rating> unseenRatings = combineRatings(userId, similarId);
			ArrayList<Movie> recommendedGenre = new ArrayList<Movie>();
			for (int itemsIndex = 0; itemsIndex < unseenRatings.size(); itemsIndex++) {
				Movie currentItem = findItem(unseenRatings.get(itemsIndex).getId());
				if ((!currentItem.equals(null)) & isGenre(genre, currentItem)) {
					recommendedGenre.set(itemsIndex, currentItem);
				}
			}
			return recommendedGenre;
		}
		ArrayList<Movie> noRecommendations = null;
		return noRecommendations;
	}

	public boolean isGenre(String genre, Movie movie) {
		String[] genreList = movie.getGenres(); // An error would arise here if
		for (int x = 0; x < genreList.length; x++) {
			if (genreList[x].equals(genre)) {
				return true;
			}
		}
		return false;
	}
	
	private Movie findItem(int itemId) {
		for (int index = 0; index < this.movies.length; index++) {
			if (this.movies[index].getId() == itemId) {
				return this.movies[index];
			}
		}
		return null;
	}

	private ArrayList<Rating> combineRatings(int userId, int similarId) {
		ArrayList<Rating> similarRatings = usersRatings.get(similarId);
		ArrayList<Rating> userIdRatings = usersRatings.get(userId);
		ArrayList<Rating> unseenRatings = new ArrayList<Rating>(similarRatings.size());
		int unseenCount = 0;
		for (int i = 0; i < similarRatings.size(); i++) {
			if (movieIsIn(similarRatings.get(i).getId(), userIdRatings)) {
				unseenRatings.add(unseenCount, similarRatings.get(i));
				//// very confused this might be wrong
			}
		}
		return unseenRatings;
	}

	private boolean movieIsIn(int movieIdSim, ArrayList<Rating> listUser) {
		for (int ratIndex = 0; ratIndex < listUser.size(); ratIndex++) {
			if (movieIdSim == listUser.get(ratIndex).getId()) {
				return false;
			}
		}
		return false;
	}
	
}