package recommendation.movies;

import java.util.ArrayList;

import recommendation.interfaces.IMovieRecommender;
import recommendation.shared.PopularRecommender;
import recommendation.shared.Rating;

/**
 * The PopularMovieRecommender class is the child class of PopularRecommender,
 * required an ArrayList<Movie>, allowing more specific methods suitable for
 * Movies
 * 
 * @author Liam Harbec
 * @date Nov. 13, 2018 
 */
public class PopularMovieRecommender extends PopularRecommender<Movie> implements IMovieRecommender {

	private ArrayList<Movie> movies;

	public PopularMovieRecommender(ArrayList<Movie> movies, ArrayList<Rating> ratings) {
		super(movies, ratings); // Calls the constructor in the PopularRecommender class
		this.movies = super.getItems();
	}

	/**
	 * Used to recommend a given number of movies the user has not rated yet
	 * 
	 * @param currentUser
	 *            The userId used to search
	 * @param number
	 *            The number of movies needed
	 * @return genreRec The list of movies corresponding to parameters
	 */
	public ArrayList<Movie> recommend(int currentUser, int number, String genre) {
		ArrayList<Movie> genreRec = new ArrayList<Movie>();
		for (int moviesIndex = 0; moviesIndex < number; moviesIndex++) {
			// System.out.println(moviesIndex);
			// System.out.println(this.movies.get(moviesIndex));
			if (!isSameUser(currentUser, this.movies.get(moviesIndex).getId())) {
				if (isGenre(genre.toLowerCase(), this.movies.get(moviesIndex))) {
					genreRec.add(this.movies.get(moviesIndex));
				}
			}
			// To ensure it doesn't pass the number
			if (moviesIndex == number || moviesIndex == this.movies.size() - 1) {
				break;
			}

		}
		return genreRec;
	}

	/**
	 * Used to identify whether or not the given movie is from the genre wanted
	 * 
	 * @param genre
	 *            The genre used to search
	 * @param movie
	 *            The movie used to search
	 * @return boolean whether or not the movie is from the genre
	 */
	public boolean isGenre(String genre, Movie movie) {
		String[] genreList = movie.getGenres();

		for (int x = 0; x < genreList.length; x++) {
			if (genreList[x].equals(genre)) {
				return true;
			}
		}
		return false;
	}

}
