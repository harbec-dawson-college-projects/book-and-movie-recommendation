/**
 * The Movie class is used to create objects of type Movie to be recommended based on a user's input
 * 
 * @author Liam Harbec
 * @author Joshua Bruder-Wexler
 * @version 1.0
 * @since Nov. 13, 2018
 */

package recommendation.movies;

import recommendation.interfaces.Item;
import recommendation.interfaces.Saveable;

/**
 * Create all the fields (Attributes) so that they can be used in other classes
 */
public class Movie implements Item, Saveable {
	private String year;
	private int id;
	private String[] genres;
	private String name;
	private String initialInput;

	/**
	 * This is the constructor, it is the way movie objects are created
	 * 
	 * @param movie
	 *            is an input given by the user in a specific format, will be
	 *            changed when we learn how to read through text files
	 */
	public Movie(String movie) {
		this.initialInput = movie;
		this.id = Integer.parseInt(movie.split(",")[0]);
		String nameYear = movie.split(",")[1];
		this.name = splitName(nameYear);
		this.year = splitYear(nameYear);
		this.genres = movie.split(",")[2].toLowerCase().split("\\|"); // Two backslashes cancel out special meaning
	}

	/**
	 * This helper method will split a String into the valid format for a name
	 * attribute
	 * 
	 * @param nameYear
	 *            A String to be split into a name attribute
	 * @return String A String that will be initialized into the attribute
	 */
	private String splitName(String nameYear) {
		int endIndex = nameYear.lastIndexOf('('); // Returns the index of the final ( to omit movies with ( in the title
		if (endIndex == -1) // If the movie does not have a (
		{
			return nameYear.trim(); // Removes whitespace
		} else {
			return nameYear.substring(0, endIndex).trim().replace("%2C", ",");
		}
	}

	/**
	 * This helper method will split a String into the valid format for a year
	 * attribute
	 * 
	 * @param nameYear
	 *            A String to be split into a year attribute
	 * @return String a String that will be initialized into the attribute
	 */
	private String splitYear(String nameYear) {
		int startIndex = nameYear.lastIndexOf('('); // Returns index of string
		int endIndex = nameYear.lastIndexOf(')');
		if (startIndex == -1 || endIndex == -1) // If the movie does not have a year
		{
			return "N/A";
		} else {
			String testYear = nameYear.substring(startIndex + 1, endIndex);
			return testYear.trim();
		}
	}

	/**
	 * This method returns the name of the movie when it is called on
	 * 
	 * @return String for name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * This method returns the year the movie was released when it is called on
	 * 
	 * @return String representing the year
	 */
	public String getYear() {
		return this.year;
	}

	/**
	 * This method returns the movie's Id number as a string
	 * 
	 * @return String representing the Id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * This method returns the movie's genres as an array of strings
	 * 
	 * @return String array representing the genres of the given movie
	 */
	public String[] getGenres() {
		String[] genresCopy = new String[genres.length];
		for (int i = 0; i < genres.length; i++) {
			genresCopy[i] = genres[i].toLowerCase(); // To ensure that genres are .equals() compatible
		}
		return genresCopy;

	}

	/**
	 * This method compares a given String "genre" to the array of genres to see if
	 * there is a match
	 * 
	 * @param genre
	 *            is a String given by the user
	 * @return true or false depending on match
	 */
	public boolean hasGenre(String genre) {
		// initialize x so that there is a return type in the end
		boolean x = false;
		// compare the given String "genre" to all array indexes. Return true if a match
		// is found
		for (int i = 0; i < genres.length; i++) {
			if (genre.equals(genres[i])) {
				x = true;
			}
		}
		// If a match is found x = true, otherwise x = false
		return x;
	}

	public String toString() {
		if (this.name.length() > 25) {
				return this.id + ", " + this.name.substring(0, 25) + "...";
		}
		return this.id + ", " + this.name;
		// Old toString below
		// return this.id + ", " + this.name + ", " + this.year + ", " + Arrays.toString(this.genres);
	}

	@Override
	public String toRawString() {
		return this.initialInput;
	}

}