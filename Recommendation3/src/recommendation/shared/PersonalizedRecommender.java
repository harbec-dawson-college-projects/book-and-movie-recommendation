/**
 * PersonalizedRecommender is used to find the similarities between all users from a given ArrayList<Rating> 
 * but will recommend to a user movies from another user which they are the most similar to depending on the ratings both users have given
 * @author Adrian Yotov
 * @author Jessica Cossio
 * @author Liam Harbec
 * @since November 13th 2018
 */

package recommendation.shared;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import recommendation.interfaces.*;

public class PersonalizedRecommender<T extends Item> implements IRecommender<T> {
	private double[][] similarUsers;
	private ArrayList<T> items;
	private HashMap<Integer, Integer> mostSimilarUsers;
	private HashMap<Integer, ArrayList<Rating>> usersRatings;

	/**
	 * Constructor to create a PersonalizedRecommender object
	 * 
	 * @param items
	 *            The unsorted items array
	 * @param ratings
	 *            The unsorted ratings array
	 * @param userNum
	 *            Used to store the amount of users a ratings[] has
	 */
	public PersonalizedRecommender(ArrayList<T> items, ArrayList<Rating> ratings) {
		// assign usersRatings as HashMap
		HashMap<Integer, ArrayList<Rating>> usersRatings = new HashMap<Integer, ArrayList<Rating>>();
		int userId=0;
		int previousIndex =0;
		for (int ratIndex = 0; ratIndex < ratings.size(); ratIndex++) {
			if (!usersRatings.containsKey(ratings.get(ratIndex).getUserId())) {
				userId=ratings.get(ratIndex).getUserId();
				ArrayList<Rating> indUserRatings= new ArrayList<>();
				for(int i=previousIndex+1;i<ratIndex;i++) {
					indUserRatings.add(ratings.get(i));
				}
				Collections.sort(indUserRatings);
				usersRatings.put(userId, indUserRatings);
				userId++;
				previousIndex=ratIndex;
			}
		}
		this.usersRatings = usersRatings;
		
		// assign similarUsers
		double similiScore=0;
		double[][] similarUsers = new double[this.usersRatings.size()][this.usersRatings.size()];//resize
		int rateCount=0;
		for (int user1 = 0; user1 < this.usersRatings.size(); user1++) {
			//containsKey is to make sure the user variables are real userIds
			if( this.usersRatings.containsKey(user1)) {
				for (int user2 = 0; user2 < this.usersRatings.size(); user2++) {
					if(user1!=user2 && this.usersRatings.containsKey(user2)) {
						//goes through all ratings of each user to find if they rated the same movie
						for(int ratIn=0; ratIn<this.usersRatings.get(user1).size();ratIn++) {
							for(int ratIn2=0; ratIn2<this.usersRatings.get(user2).size();ratIn2++) {
								if(this.usersRatings.get(user1).get(ratIn).getId()==this.usersRatings.get(user2).get(ratIn2).getId()) {
									rateCount++;
									similiScore += (this.usersRatings.get(user1).get(ratIn).getRating()-2.5)*(this.usersRatings.get(user2).get(ratIn2).getId()-2.5);
								}
							}
						}
					}
					similarUsers[user1][user2] = similiScore/rateCount;
					similiScore=0;
					rateCount=0;
				}
			}
		}
		this.similarUsers = similarUsers;
		
		// Assign mostSimilarUsers as HashMap
		HashMap<Integer, Integer> mostSimilarUsers = new HashMap<Integer, Integer>();
		for (int userId1 = 0; userId1 < this.similarUsers.length; userId1++) {
			int mostSimilarId=-1;
			for(int i=0;i>this.similarUsers[userId1].length;i++) {
				if (this.similarUsers[userId1][i]>this.similarUsers[userId1][mostSimilarId]&&this.similarUsers[userId1][i]>-1) {
					mostSimilarId=i;
				}
			}
			mostSimilarUsers.put(userId1, mostSimilarId);
			mostSimilarId=-1;
		}
		this.mostSimilarUsers = mostSimilarUsers;
	}
/*
	/**
	 * userCheck is a helper method for checking the amount of users in an ArrayList<Rating>
	 * 
	 * @param a
	 *            The unsorted ratings array
	 * @param differentUsers
	 *            Used to store the amount of users an ArrayList<Rating> has
	 * @return userIds returns the userIds[][]
	 *
	/*public static int userCheck(ArrayList<Rating> ratings) {
		int differentUsers = 1;
		for (int i = 0; i < ratings.size() - 1; i++) {
			if (ratings.get(i).getUserId() != ratings.get(i+1).getUserId()) {
				differentUsers++;
			}
		}
		return differentUsers;
	}

	// method compares rating of two given users and will return their similarity
	// score

	/**
	 * compareRatings is a helper method to compare ratings between two users
	 * 
	 * @param ratings
	 *            The unsorted ratings array
	 * @param user1
	 *            User to compare to user2
	 * @param user2
	 *            User being compared to user1
	 * @param similarityScore
	 *            The score user1 has when compared to user2
	 * @return similarityScore Returns the score user1 has when compared to user2
	 
	/*public double compareRatings(ArrayList<Rating> ratings, int user1, int user2) {
		double similarityScore = 0;
		ArrayList<Rating> user1Ratings = findUserRatings(ratings, user1);
		ArrayList<Rating> user2Ratings = findUserRatings(ratings, user2);
		for (int i = 0; i < user1Ratings.size(); i++) {
			for (int j = 0; j < user2Ratings.size(); j++) {
				if (user1Ratings.get(i).getId() == user2Ratings.get(j).getId()) {
					similarityScore = similarityScore + (user1Ratings.get(i).getRating() * user2Ratings.get(j).getRating());
				}
			}
		}
		return similarityScore;
	}

	/**
	 * findUserRatings is a helper method to return an array of a given user's
	 * ratings
	 * 
	 * @param ratings
	 *            The unsorted ratings array
	 * @param userId
	 *            The user's ID is used to find their ratings
	 * @param currentIndex
	 *            Keeps track of how many ratings a user has
	 * @param tempRatings
	 *            An array of fixed (10000) size holding the user's ratings
	 * @return userRatings Will return the user's ratings after being transferred
	 *         from tempRatings
	 *
	/*public ArrayList<Rating> findUserRatings(ArrayList<Rating> ratings, int userId) {
		ArrayList<Rating> tempRatings = new ArrayList<Rating>();
		for (int i = 0; i < ratings.size(); i++) {
			if (ratings.get(i).getUserId() == userId) {
				tempRatings.add(ratings.get(i));
			}
			if (i == ratings.size() - 1) {
				break;
			}
		}
		ArrayList<Rating> userRatings = new ArrayList<Rating>();
		for (int i = 0; i < userRatings.size(); i++) {
			if (tempRatings.get(i) == null) {
				break;
			} else {
				userRatings.set(i, tempRatings.get(i));
			}
		}
		return userRatings;
	}

	/**
	 * mostSimilar is a helper method to see which user is the most similar to a
	 * user
	 * 
	 * @param a
	 *            The unsorted ratings array
	 * @param user1
	 *            The user that is being compared to others
	 * @param nextScore
	 *            Stores the similarity score of the user user1 is being compared to
	 * @param similarityScore
	 *            Stores the highest similarityScore between user1 and a certain
	 *            user in the ArrayList<Rating>
	 * @param userIndex
	 *            Stores the index where user1 had the highest score to the user
	 *            they are being compared to
	 * @return userIndex Returns the index of the user with the highest similarity
	 *         score for user1
	 *
	/*public int mostSimilar(ArrayList<Rating> ratings, int user1) {
		double nextScore;
		double similarityScore = 0;
		int userIndex = 0;
		double[][] users = userSimilarity(ratings);
		for (int i = 0; i < users.length; i++) {
			if (i != user1) {
				nextScore = compareRatings(ratings, user1, i);
			}
			if (similarityScore > nextScore) {
				continue;
			} else if (similarityScore < nextScore) {
				similarityScore = nextScore;
				userIndex = i;
			}
		}
		return userIndex;
	}*/
	/**
	 * recommend is a method used to recommend a given number of movies the user has
	 * not rated yet from the mostSimilar user's ratings
	 * 
	 * @param userId
	 *            The userId to search for
	 * @param numWanted
	 *            The number of movies needed
	 * @return movie[] The list of movies corresponding to parameters
	 */
	public ArrayList<T> recommend(int userId, int numWanted) {
		//System.out.println(this.mostSimilarUsers.get(15));
		int similarId = this.mostSimilarUsers.get(userId);
		if (similarId != -1) {
			ArrayList<Rating> unseenRatings = combineRatings(userId, similarId);
			ArrayList<T> topRec = new ArrayList<T>();
			int currentNumb = 0;
			int itemsIndex = 0;
			for (int ratIndex = 0; ratIndex < unseenRatings.size(); ratIndex++) {
				
				if (currentNumb < numWanted) {
					if (movieIsIn(unseenRatings.get(ratIndex).getId(), usersRatings.get(userId))) {
						topRec.add(this.items.get(itemsIndex));
						currentNumb++;
						itemsIndex++;
					}
				} else {
					return topRec;
				}
			}
		}
		ArrayList<T> noRecommendations = new ArrayList<T>();
		return noRecommendations;
	}

	private ArrayList<Rating> combineRatings(int userId, int similarId) {
		ArrayList<Rating> similarRatings = usersRatings.get(similarId);
		ArrayList<Rating> userIdRatings = usersRatings.get(userId);
		ArrayList<Rating> unseenRatings = new ArrayList<Rating>();
		for (int i = 0; i < similarRatings.size(); i++) {
			if (movieIsIn(similarRatings.get(i).getId(), userIdRatings)) {
				unseenRatings.add(similarRatings.get(i));
			}
		}
		return unseenRatings;
	}

	private boolean movieIsIn(int movieIdSim, ArrayList<Rating> listUser) {
		for (int ratIndex = 0; ratIndex < listUser.size(); ratIndex++) {
			if (movieIdSim == listUser.get(ratIndex).getId()) {
				return true;
			}
		}
		return false;
	}

	/*/**
	 * userSimilarity is a helper method to compare ratings between two users
	 * 
	 * @param ratings
	 *            The unsorted ratings array
	 * @param users
	 *            The number of users in a ratings file
	 * @return similarUsers Returns the users with their similarity scores towards
	 *         all other users
	 */
	/*private double[][] userSimilarity(ArrayList<Rating> ratings) {
		//int users = userCheck(ratings);
		double similiScore=-1;
		double[][] similarUsers = new double[700][700];//resize
		for (int user1 = 0; user1 < similarUsers.length; user1++) {
			int userIndex=0;
			for (int user2 = 0; user2 < similarUsers.length; user2++) {
				if(user1!=user2) {
					if(ratings.get(user1).getId()==ratings.get(user2).getId()) {
						similiScore += (ratings.get(user1).getRating() * ratings.get(user2).getRating());
						similarUsers[user1][userIndex] = ratings.get(user2).getUserId();
						similiScore=-1;
						userIndex++;
					}
				}
				else{
					similarUsers[user1][user2] = similiScore;
				}
			}
		}
		return similarUsers;
	}*/
	
	public HashMap<Integer, Integer> getMostSimilarUsers() {
		return this.mostSimilarUsers;
	}

	public HashMap<Integer, ArrayList<Rating>> getUsersRatings() {
		return this.usersRatings;
	}
}
