package recommendation.shared;

import java.util.ArrayList;
import recommendation.interfaces.*;
import recommendation.shared.ItemRating;

/**
 * The PopularRecommender object will be used to have access to a sorted ArrayList<T>
 * and ArrayList<Rating>, and have methods used for recommendation purposes
 * 
 * @author Liam Harbec
 * @author Jessica Cossio
 * @date Nov. 13, 2018
 */
public class PopularRecommender<T extends Item> implements IRecommender<T> {

	private ArrayList<T> items;
	private ArrayList<Rating> ratings;

	/**
	 * Constructor to create a PopularRecommender object
	 * 
	 * @param items
	 *            The unsorted type T ArrayList
	 * @param ratings
	 *            The unsorted ratings array
	 */
	public PopularRecommender(ArrayList<T> items, ArrayList<Rating> ratings) {
		// Groups up movieId's together
		ratings = groupByMovie(ratings);

		// Creates a double[] of item rating averages, corresponds to ratingsCopy id
		double[] averages = calculateRatingAverages(items.size(), ratings);

		// Sorts Movie's by increasing movieId
		sortById(items);

		// Creates an array of ItemRating type
		ArrayList<ItemRating> itemRatings = makeItemRatings(items, averages);

		// Initializing the items field
		this.items = items;
		// Sorts Movie's by decreasing order based on their average rating
		sortByDecreasing(itemRatings);

		// Initializing the ratings field
		this.ratings = ratings;
	}

	/**
	 * Used to group up the contents of a ratings array by the id field
	 * 
	 * @param ratings
	 *            The non-grouped ratings array
	 * @return ratings The grouped ratings array
	 */
	public static ArrayList<Rating> groupByMovie(ArrayList<Rating> ratings) {
		int length = ratings.size();
		Rating temp; // Temporary Rating variable, used for swapping

		// Bubble sort to group ratings by movieId attribute
		for (int i = 0; i < length; i++) {
			for (int j = 1; j < length - i; j++) {
				// parseInt since movieId field is a String
				if (ratings.get(j-1).getId() > ratings.get(j).getId()) {
					temp = ratings.get(j-1);
					ratings.set(j-1, ratings.get(j));
					ratings.set(j, temp);
				}
			}
		}
		return ratings;
	}

	/**
	 * Used to calculate a ratings array by the id field
	 * 
	 * @int length The length of the movies array used for a corresponding averages
	 *      length
	 * @param ratings
	 *            The non-grouped ratings array
	 * @return double The grouped ratings array
	 */
	public static double[] calculateRatingAverages(int length, ArrayList<Rating> ratings) {
		double[] averages = new double[length];
		double average = 0;
		int count = 0;
		double sum = 0;
		int skipRating = 0; // Used to store j's value
		for (int i = 0; i < length; i++) {
			for (int j = skipRating; j < ratings.size(); j++) {
				if (j == ratings.size() - 1) {
					sum += ratings.get(j).getRating();
					count++;
					break;
				} else if (ratings.get(j).getId() == (ratings.get(j+1).getId())) {
					sum += ratings.get(j).getRating();
					count++;
					skipRating++;
				} else {
					sum += ratings.get(j).getRating();
					count++;
					skipRating++;
					break;
				}
			}
			average = sum / count; // Get the average of one movie
			averages[i] = average;
			sum = 0; // Reset values for next movie's average calculation
			count = 0;
		}
		return averages;
	}

	/**
	 * Used to sort an items ArrayList by the id field
	 * 
	 * @param items
	 *            The unsorted type T ArrayList
	 * @return No return as the array gets automatically sorted
	 */
	public void sortById(ArrayList<T> items) {
		int length = items.size();
		T temp; // Temporary variable, used for swapping

		// Bubble sort based on id
		for (int i = 0; i < length; i++) {
			for (int j = 1; j < length - i; j++) {
				if ((items.get(j - 1)).getId() > (items.get(j)).getId()) {
					temp = items.get(j - 1);
					items.set(j - 1, items.get(j));
					items.set(j, temp);
				}
			}
		}
	}

	/**
	 * Used to make an ArrayList<ItemRating> from the two parameters
	 * 
	 * @param items
	 *            The unsorted type T ArrayList
	 * @param averages
	 *            The unsorted movie averages
	 * @return itemRatings The unsorted ArrayList<ItemRating>
	 */
	public ArrayList<ItemRating> makeItemRatings(ArrayList<T> items, double[] averages) {
		ArrayList<ItemRating> itemRatings = new ArrayList<ItemRating>();

		// Adds an item rating from each index
		for (int i = 0; i < items.size(); i++) {
			itemRatings.add(new ItemRating(items.get(i), averages[i]));
		}
		return itemRatings;
	}

	/**
	 * Used to sort an ArrayList<ItemRating> by decreasing order based on rating
	 * from highest to lowest
	 * 
	 * @param itemRatings
	 *            The ArrayList<ItemRating> that will be sorted
	 * @return movies The sorted Movie[] in decreasing order by rating
	 */
	public void sortByDecreasing(ArrayList<ItemRating> itemRatings) {
		int length = itemRatings.size();
		ItemRating tempIR; // Temporary MovieRating variable, used for swapping

		// Bubble sort of itemRatings by highest to lowest average rating
		for (int i = 0; i < length; i++) {
			for (int j = 1; j < length - i; j++) {
				if (itemRatings.get(j - 1).getAverageRating() < itemRatings.get(j).getAverageRating()) {
					tempIR = itemRatings.get(j - 1);
					itemRatings.set(j - 1, itemRatings.get(j));
					itemRatings.set(j, tempIR);
				}
			}
		}

		T tempT;
		for (int i = 0; i < length; i++) {
			for (int j = 0; j < length; j++) {
				if (itemRatings.get(i).getName().equals(this.items.get(j).getName())) {
					tempT = this.items.get(i);
					this.items.set(i, this.items.get(j));
					this.items.set(j, tempT);
				}
			}
		}
	}

	/**
	 * Used to recommend a given number of Item type T the user has not rated yet
	 * 
	 * @param currentUser
	 *            The userId used to search
	 * @param count
	 *            The number of movies needed
	 * @return topRec The list of Item type T corresponding to parameters
	 */
	public ArrayList<T> recommend(int currentUser, int count) {
		ArrayList<T> topRec = new ArrayList<T>();

		int currentNumb = 0;
		int moviesIndex = 0;
		while (currentNumb < count) {
			if (moviesIndex >= this.items.size()) {
				return topRec;
			}
			if (!isSameUser(currentUser, this.items.get(moviesIndex).getId())) {
				topRec.add(currentNumb, this.items.get(moviesIndex));
				currentNumb++;
			}
			moviesIndex++;
		}
		return topRec;
	}

	/**
	 * Used to identify whether or not the user rated a given Item of type T
	 * 
	 * @param currentUser
	 *            The userId used to search
	 * @param titleId
	 *            The id of Item type T used to compare
	 * @return boolean Whether or not the user rated the given movie
	 */
	public boolean isSameUser(int currentUser, int titleId) {
		for (int ratesIndex = 0; ratesIndex < this.ratings.size(); ratesIndex++) {
			// Returns true if they match
			if (this.ratings.get(ratesIndex).getUserId() == currentUser && this.ratings.get(ratesIndex).getId() == titleId) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Getter method, used to return the Item type T attribute for test class
	 * 
	 * @return movies The initialized movies property
	 */
	public ArrayList<T> getItems() {
		return this.items;
	}

	/**
	 * Getter method, used to return ratings attribute for test class
	 * 
	 * @return ratings The initialized ratings property
	 */
	public ArrayList<Rating> getRatings() {
		return this.ratings;
	}

}
