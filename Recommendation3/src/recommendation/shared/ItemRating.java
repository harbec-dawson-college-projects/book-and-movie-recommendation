package recommendation.shared;

import recommendation.interfaces.Item;

/**
 * The ItemRating class creates a MovieRating object which will store an Item
 * object and its average rating
 * 
 * @author Liam Harbec
 * @date Nov. 13, 2018
 */
public class ItemRating {

	private Item item;
	private double averageRating;

	/**
	 * Constructor to create a ItemRating object
	 * 
	 * @param item
	 *            The given Item
	 * @param averages
	 *            The given Item's average rating
	 */
	public ItemRating(Item item, double average) {
		this.item = item;
		this.averageRating = average;
	}

	/**
	 * Used to get an item attribute
	 * 
	 * @return item The item attribute
	 */
	public Item getItem() {
		return this.item;
	}

	/**
	 * Used to get a Item's average rating
	 * 
	 * @return movie The Item's average rating attribute
	 */
	public double getAverageRating() {
		return this.averageRating;
	}

	/**
	 * Used to get a Item's name
	 * 
	 * @return item.getName()
	 */
	public String getName() {
		return this.item.getName();
	}

	/**
	 * An Overriden toString() method
	 * 
	 * @return String The item's attributes in one String
	 */
	@Override
	public String toString() {
		return "name=" + this.item.getName() + ", averageRating=" + averageRating + "]";
	}
}
