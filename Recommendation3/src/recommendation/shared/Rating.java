/**
 * 
 */
package recommendation.shared;

import java.util.Arrays;

import recommendation.interfaces.Saveable;

/**
 * The Rating type is used to sort Items based on popularity, or most recommended based on other ratings
 * 
 * @author Adrian Yotov
 * @date Nov. 13, 2018
 * @version 1.0
 *
 */
public class Rating implements Comparable<Rating> , Saveable{
	private int userId;
	private int itemId;
	private double rating;
	private String initialInput;

	/**
	 * Constructor for a Rating object
	 * 
	 * @param rating
	 *            used for initializing the userId, movieId and rating fields
	 * 
	 */
	public Rating(String rating) {
		this.initialInput=rating;
		String[] ratingToSplit = rating.split(",");
		this.userId = Integer.parseInt(ratingToSplit[0]);
		this.itemId = Integer.parseInt(ratingToSplit[1]);
		this.rating = Double.parseDouble(ratingToSplit[2]); // Converts a String to a double
	}

	/**
	 * Gets the reviewer's Id
	 * 
	 * @return userId returns the reviewers' Id
	 * 
	 */
	public int getUserId() {
		return this.userId;
	}

	/**
	 * Gets the movie's Id
	 * 
	 * @return movieId returns the movie's Id
	 * 
	 */
	public int getId() {
		return this.itemId;
	}

	/**
	 * Gets the rating of a specific review
	 * 
	 * @return rating returns the rating of a review
	 * 
	 */
	public double getRating() {
		return this.rating;
	}
	

	public int compare(Rating rate1, Rating rate2){
		return rate1.compareTo(rate2);
	}

	@Override
	public int compareTo(Rating rate2) {
		if(this.getRating()>rate2.getRating()){
			return 1;
		}else if (this.getRating()<rate2.getRating()){
			return -1;
		}
		return 0;
	}
	
	@Override
	public String toRawString() {
		return this.initialInput;
	}

}
