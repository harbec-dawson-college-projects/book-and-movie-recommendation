/**
 * The ItemRating class creates a MovieRating object which will store an Item
 * object and its average rating
 * 
 * @author Jessica Cossio
 * @date Dec. 7, 2018
 */
package recommendation.shared;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import recommendation.interfaces.Saveable;

public class SaveUtilities<T extends Saveable> {
	
	/**
	 * Saves object input into a file
	 * 
	 * @param objects
	 * 			ArrayList of the items to print
	 * @param path
	 * 			Path of the file in which to write
	 * @param fileHeader
	 * 			Header for the new file
	 */
	public void saveToFile(ArrayList<T> objects, String path, String fileHeader) {
		try {
			//ByteArrayOutputStream byteOut= new	ByteArrayOutputStream();
			//ObjectOutputStream oos = new ObjectOutputStream(byteOut);
			FileOutputStream fos = new FileOutputStream(path);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			StringBuilder fileInput = new StringBuilder();
			fileInput.append(fileHeader + "\n");
			for(int i = 0; i < objects.size(); i++) {
				fileInput.append(objects.get(i).toRawString() + "\n");	
			}
			String input = fileInput.toString();
			for (int i = 0; i < 10; i++) {
				oos.writeObject(input);
			}
			oos.close();
		} catch (IOException e) {
			throw new IllegalArgumentException("The path is invalid");
		}
	}
}
