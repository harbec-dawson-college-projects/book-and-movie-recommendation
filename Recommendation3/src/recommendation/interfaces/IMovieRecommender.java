package recommendation.interfaces;

import java.util.ArrayList;

import recommendation.movies.Movie;


/**
 * The IMovieRecommender interface is used to define a set recommend method
 * 
 * @author Liam Harbec
 * @version 1.0
 * @since Nov. 13, 2018
 */
public interface IMovieRecommender extends IRecommender<Movie>
{
	ArrayList<Movie> recommend(int userId, int n, String genre);
}
