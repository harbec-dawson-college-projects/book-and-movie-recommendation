package recommendation.interfaces;

import java.util.ArrayList;

/**The IRecommender interface is used to define a method to be used
 * by various systems of recommendation
 * 
 * @author Liam Harbec
 * @date   Nov. 13, 2018
 */
public interface IRecommender<T extends Item>
{
	ArrayList<T> recommend(int userId, int n);
}
