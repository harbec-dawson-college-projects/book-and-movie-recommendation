/**
 * The Item interface is used to define two set methods for anything that will implement it
 * 
 */
package recommendation.interfaces;

/**
 * @author Adrian Yotov
 *
 */
public interface Item {

	String getName();
	int getId();
}
