package recommendation.interfaces;

public interface Saveable {
	public String toRawString();
}
