package recommendation.book;

import java.util.Arrays;

import recommendation.interfaces.Item;
import recommendation.interfaces.Saveable;

/**
 * The Book class is used to create objects of type Book to be recommended based
 * on a user's input
 * 
 * @author Liam Harbec
 * @author Joshua Bruder-Wexler
 * @version 1.0
 * @since Nov. 13, 2018
 */
public class Book implements Item, Saveable {
	private int id;
	private String name;
	private String[] authors;
	private String isbn;
	private String initialInput;

	/**
	 * The contructor for a Book that initializes all of the attributes
	 * 
	 * @param String
	 *            A book stored in a String from a table
	 */
	public Book(String book) {
		this.initialInput = book;
		String[] details = book.split(",");

		// As there are 23 fields in the Book file
		if (details.length != 23) {
			throw new IllegalArgumentException("The format of this Book is incorrect");
		}
		id = Integer.parseInt(details[0]);
		isbn = details[5];
		authors = details[7].trim().split("%2C"); // trim() removes whitespace, %2C is a ,
		name = details[10].replace("%2C", ",");
	}

	/**
	 * A getter method that returns the id attribute of a Book
	 * 
	 * @return int The id attribute
	 */
	public int getId() {
		return id;
	}

	/**
	 * A getter method that returns the name attribute of a Book
	 * 
	 * @return String The name attribute
	 */
	public String getName() {
		return name;
	}

	/**
	 * A getter method that returns the authors attribute of a Book
	 * 
	 * @return String[] the authors attribute
	 */
	public String[] getAuthors() {
		String[] authorsCopy = new String[authors.length];
		for (int i = 0; i < authors.length; i++) {
			authorsCopy[i] = authors[i];
		}

		return authorsCopy;

	}

	/**
	 * A getter method that returns the isbn attribute of a Book
	 * 
	 * @return String the isbn attribute
	 */
	public String getIsbn() {
		return isbn;
	}

	@Override
	public String toString() {
		if (this.name.length() > 25) {
			// Additional spaces to make the ComboBox size align with default and with
			// movies
			return this.id + ", " + this.name.substring(0, 25) + "...           ";
		}
		return this.id + ", " + this.name;
		// return this.id + ", " + this.name + ", ISBN: " + this.isbn + ", " + Arrays.toString(this.authors) + "\n";
	}

	@Override
	public String toRawString() {
		return this.initialInput;
	}
	// public Book(String book) {
}
