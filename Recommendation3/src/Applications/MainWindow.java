/**
 * The MainWindow class creates a window which will display the recommendations
 * 
 * @author Liam Harbec
 * @author Jessica Cossio
 * @date Dec. 7, 2018
 */
package Applications;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import recommendation.book.Book;
import recommendation.fileio.GoodReadsFileReader;
import recommendation.fileio.ItemFileReader;
import recommendation.fileio.MovieLensFileReader;
import recommendation.interfaces.IMovieRecommender;
import recommendation.interfaces.IRecommender;
import recommendation.interfaces.Item;
import recommendation.movies.Movie;
import recommendation.movies.PersonalizedMovieRecommender;
import recommendation.movies.PopularMovieRecommender;
import recommendation.shared.PersonalizedRecommender;
import recommendation.shared.PopularRecommender;
import recommendation.shared.Rating;
import recommendation.shared.SaveUtilities;

public class MainWindow<T extends Item, Saveable> extends Application {

	private ArrayList<Movie> movies;
	private ArrayList<Book> books;
	private ArrayList<Rating> ratings;
	private boolean itemSelected; // Where true == Movie, false == Book
	private String itemPath;
	private String ratingsPath;
	private TextField itemsTextField;
	private TextField ratingsTextField;
	SaveUtilities<Movie> movieSave = new SaveUtilities<Movie>();
	SaveUtilities<Book> bookSave = new SaveUtilities<Book>();
	SaveUtilities<Rating> ratingSave = new SaveUtilities<Rating>();
	private int ratingSelect;
	private TextField userIdTextField;
	private int userId;
	private Movie movie;
	private Book book;
	private Label ratingMessage;
	private ComboBox<String> tempCB;
	private ComboBox<Movie> movieCB;
	private ComboBox<Book> bookCB;
	private Button addRating;
	private VBox ratingsVBox;
	private HBox ratingsHBox;
	private VBox input;
	private VBox results;
	private boolean buttonSelect; // Where true == popular, false == personalized
	private Button goButton;
	private VBox itemsVBox;
	private HBox genre;
	private TextField genreSelection;
	private TextArea recommendedResults;

	/**
	 * Creates the scene and specifies the background image
	 * 
	 *  @param stage
	 *            The stage for the window
	 */
	@Override
	public void start(Stage stage) throws Exception {

		StackPane group = new StackPane();
		BorderPane bp = new BorderPane();

		String imageUrl = "https://i.imgur.com/TrV3rYJ.png";

		BackgroundSize bgs = new BackgroundSize(.5, .4, true, true, false, false);
		BackgroundImage bg = new BackgroundImage(new Image(imageUrl), BackgroundRepeat.REPEAT,
				BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT, bgs);
		
		group.setBackground(new Background(bg));

		// Making the scene with these dimensions
		Scene mainWindow = new Scene(group, 700, 500);

		// Initializing the content
		inputLayout(group);
		resultsLayout(group);

		// Making mainWindow appear
		stage.setTitle("Recommendation System");
		stage.setScene(mainWindow);
		stage.show();
		group.getChildren().add(bp);
		bp.setLeft(this.input);
		bp.setRight(this.results);
	}

	/**
	 * Creates the Vbox where all the user's input goes
	 * 
	 * @param group
	 * 			Vbox goes in the StackPane
	 */
	private void inputLayout(StackPane group) {
		VBox input = new VBox();
		group.getChildren().add(input);
		// Makes path VBox
		pathsVBox(input);

		// Makes item VBox
		itemsVBox(input);
		// Makes rating VBox
		ratingsVBox(input);

		input.setPadding(new Insets(10, 0, 0, 10));
		input.setSpacing(50);
		this.input = input;
	}

	/**
	 * Creates and fills the first section of the user's input (Vbox paths)
	 * @param group
	 * 			Vbox paths is its child
	 */
	private void pathsVBox(VBox group) {
		// paths VBox creation
		VBox paths = new VBox();

		HBox items = new HBox();
		RadioButton movie = new RadioButton("Movie");
		movie.setSelected(true);
		RadioButton book = new RadioButton("Book");
		items.getChildren().add(movie);
		items.getChildren().add(book);

		ToggleGroup itemsToggle = new ToggleGroup();
		movie.setToggleGroup(itemsToggle);
		book.setToggleGroup(itemsToggle);
		movie.setOnAction(this::movieIsChecked);
		book.setOnAction(this::bookIsChecked);

		paths.getChildren().add(items);
		group.getChildren().add(paths);

		pathsInput(paths);
		loadSaveButtons(paths);

		items.setSpacing(10);
		paths.setSpacing(10);
	}

	/**
	 * Specifies that movie is checked and adds genres
	 * 
	 * @param e
	 * 		movie radioButton is checked
	 */
	private void movieIsChecked(ActionEvent e) {
		// true to represent movie
		this.itemSelected = true;
		this.itemsVBox.getChildren().remove(this.goButton);
		this.itemsVBox.getChildren().add(this.genre);
		this.itemsVBox.getChildren().add(this.goButton);
	}

	/**
	 * Specifies that book is checked and removes genres
	 * @param e
	 * 		book radioButton is checked
	 */
	private void bookIsChecked(ActionEvent e) {
		// false to represent book
		this.itemSelected = false;
		this.itemsVBox.getChildren().remove(this.genre);
	}

	/**
	 * Creates load and save buttons
	 * 
	 * @param paths
	 * 		Vbox storing the paths
	 */
	private void loadSaveButtons(VBox paths) {
		HBox buttons = new HBox();

		// The value is initially checked, though it does not work without manually
		// declaring it
		this.itemSelected = true;

		// Load button stuff
		Button loadButton = new Button("Load");
		loadButton.setOnAction(this::loadArrays);
		buttons.getChildren().add(loadButton);

		// Save button stuff
		Button saveButton = new Button("Save");
		saveButton.setOnAction(this::saveToFile);
		buttons.getChildren().add(saveButton);

		paths.getChildren().add(buttons);
		buttons.setSpacing(10);
	}

	/**
	 * Adds item list to comboBox if paths are valid
	 * 
	 * @param e
	 * 		when load button is pressed
	 */
	private void loadArrays(ActionEvent e) {
		this.itemPath = itemsTextField.getText();
		this.ratingsPath = ratingsTextField.getText();

		// Will execute if the type is a Movie
		if (this.itemSelected) {
			try {
				this.movies = MovieLensFileReader.loadMovies(this.itemPath);
			} catch (IOException exception) {
				throw new IllegalArgumentException("Please enter a valid path for a movies file");
			}
		}

		// Will execute if the type is a Book
		else {
			try {
				this.books = GoodReadsFileReader.loadBooks(this.itemPath);
			} catch (IOException exception) {
				throw new IllegalArgumentException("Please enter a valid path for a movies file");
			}
		}

		// Loads ratings to a ArrayList<Rating>, respective to which Item type it is
		try {
			this.ratings = ItemFileReader.loadRatings(ratingsPath);
		} catch (IOException exception) {
			if (itemSelected) {
				throw new IllegalArgumentException("Please enter a valid movie ratings path");
			} else {
				throw new IllegalArgumentException("Please enter a valid book ratings path");
			}
		}
		itemsComboBox(this.ratingsVBox);
	}

	/**
	 * Saves items and ratings into a copy file
	 * 
	 * @param e
	 * 		when save button is pressed
	 */
	private void saveToFile(ActionEvent e) {

		// If the movie button is selected
		if (this.itemSelected) {
			String movieRatingCopyPath = new File("datafiles/sorted/movie_ratings_copy.csv").getAbsolutePath();
			System.out.println(movieRatingCopyPath);
			this.movieSave.saveToFile(this.movies, this.itemPath, "movieId,title,genres");
			this.ratingSave.saveToFile(this.ratings, movieRatingCopyPath, "userId,movieId,rating,,");
		}
		// If the book button is selected
		else {
			String bookRatingCopyPath = new File("datafiles/sorted/book_ratings_copy.csv").getAbsolutePath();
			this.bookSave.saveToFile(this.books, this.itemPath,
					"book_id,goodreads_book_id,best_book_id,work_id,books_count,isbn,isbn13,authors,original_publication_year,original_title,title,language_code,average_rating,ratings_count,work_ratings_count,work_text_reviews_count,ratings_1,ratings_2,ratings_3,ratings_4,ratings_5,image_url,small_image_url");
			this.ratingSave.saveToFile(this.ratings, bookRatingCopyPath, "user_Id,book_Id,rating,,");
		}
	}

	/**
	 * Creates textBoxes and labels for paths
	 * 
	 * @param paths
	 * 		paths' children
	 */
	private void pathsInput(VBox paths) {
		HBox items = new HBox();
		Label itemLabel = new Label("Item Path:  ");
		TextField itemPath = new TextField();
		items.getChildren().add(itemLabel);
		items.getChildren().add(itemPath);

		HBox ratings = new HBox();
		Label ratingLabel = new Label("Rating Path:  ");
		TextField ratingsPath = new TextField();
		ratings.getChildren().add(ratingLabel);
		ratings.getChildren().add(ratingsPath);

		this.itemsTextField = itemPath;
		this.ratingsTextField = ratingsPath;
		paths.getChildren().add(items);
		paths.getChildren().add(ratings);
	}

	/**
	 * Creates Vbox that where user can input
	 * id, genres and choose from personalized/popular and the item
	 * 
	 * @param group
	 * 		parent for itemsVbox
	 */
	private void itemsVBox(VBox group) {
		VBox itemsVBox = new VBox();

		// The value is initially checked, though it does not work without manually
		// declaring it
		this.buttonSelect = true;

		HBox userId = new HBox();
		Label userIdLabel = new Label("User ID:  ");
		TextField userIdText = new TextField();
		this.userIdTextField = userIdText;
		userId.getChildren().add(userIdLabel);
		userId.getChildren().add(userIdText);
		this.userIdTextField = userIdText;

		HBox recommenderHB = new HBox();
		recommenderHB.setSpacing(10);
		HBox genre = new HBox();
		genre.setSpacing(10);
		// To group ratings and only make one selectable at a time
		ToggleGroup recommender = new ToggleGroup();

		// Creating the buttons
		RadioButton popular = new RadioButton("Popular");
		popular.setSelected(true);
		RadioButton personalized = new RadioButton("Personalized");

		// Grouping them to ratings ToggleGroup
		popular.setToggleGroup(recommender);
		personalized.setToggleGroup(recommender);

		// Adds them to the recommenderHBox
		recommenderHB.getChildren().add(popular);
		recommenderHB.getChildren().add(personalized);

		// Creates Go and Genre features
		Button recommend = new Button("Go!");
		recommend.setOnAction(this::goClick);
		Label genreLabel = new Label("Genre:");
		TextField genreTextField = new TextField();
		this.genre = genre;
		this.genreSelection = genreTextField;

		// Adds Go and Genre features
		genre.getChildren().add(genreLabel);
		genre.getChildren().add(genreTextField);

		// Adds all features to the itemsVBox
		itemsVBox.getChildren().add(userId);
		itemsVBox.getChildren().add(recommenderHB);
		itemsVBox.getChildren().add(genre);
		itemsVBox.getChildren().add(recommend);

		// Adds the itemsVBox to the full group
		group.getChildren().add(itemsVBox);
		itemsVBox.setSpacing(10);

		// Handlers for the recommender buttons
		popular.setOnAction(this::popularSelect);
		personalized.setOnAction(this::personalizedSelect);

		this.goButton = recommend;
		this.itemsVBox = itemsVBox;
	}

	/**
	 * Creates ArrayList<T> of recommended items
	 * 
	 * @param e
	 * 		when Go button is pressed
	 */
	private void goClick(ActionEvent e) {
		// If it's a Movie
		if (this.itemSelected) {
			IMovieRecommender recommended = null;
			// If it's popular
			if (this.buttonSelect) {
				recommended = new PopularMovieRecommender(this.movies, ratings);
				if (this.genreSelection.getText().equals("all") || this.genreSelection.getText().equals("")) {
					// The variable will store 10 as this is a test to ensure that it works
					print(recommended.recommend(this.userId, 10));
				} else {
					print(recommended.recommend(this.userId, 10, this.genreSelection.getText()));
				}
			}
			// If it's personalized
			else {
				recommended = new PersonalizedMovieRecommender(this.movies, ratings);

				// Prompting user for a genre and a userid
				if (this.genreSelection.getText().equals("all") || this.genreSelection.getText().equals("")) {
					// The variable will store 10 as this is a test to ensure that it works
					print(recommended.recommend(this.userId, 10));
				} else {
					print(recommended.recommend(this.userId, 10, this.genreSelection.getText()));
				}
			}
		}
		// If it's a Book
		else {
			IRecommender recommender = null;
			// If it's popular
			if (this.buttonSelect) {
				recommender = new PopularRecommender(this.books, ratings);
				// The variable will store 10 as this is a test to ensure that it works
				print(recommender.recommend(this.userId, 10));
			}
			// If it's personalized
			else {
				recommender = new PersonalizedRecommender(this.books, ratings);
				// The variable will store 10 as this is a test to ensure that it works
				print(recommender.recommend(this.userId, 10));
			}
		}
	}

	/**
	 * Prints recommendations
	 * 
	 * @param array
	 * 		Items to print
	 */
	public <T> void print(ArrayList<T> array) {
		if (array.isEmpty()) {
			this.recommendedResults
					.setText("\n\n\n\n\n\n\n\n\nThere are no recommendations available\n for this user!");
		} else {
			StringBuilder text = new StringBuilder();
			int index = 0;
			for (T i : array) {
				// To avoid newlines in last result
				if (index == array.size() - 1) {
					text.append(i);
				} else {
					text.append(i + "\n\n");
					index++;
				}
			}
			String results = text.toString();
			System.out.println(results);
			this.recommendedResults.setText(results);
		}
	}

	/**
	 * Specifies that popular is checked
	 * 
	 * @param e
	 * 		popular radioButton is checked
	 */
	private void popularSelect(ActionEvent e) {
		this.buttonSelect = true;
	}
	
	/**
	 * Specifies that personalized is checked
	 * 
	 * @param e
	 * 		personalized radioButton is checked
	 */
	private void personalizedSelect(ActionEvent e) {
		this.buttonSelect = false;
	}

	/**
	 * Creates and fills the ratings section of the user's input (Vbox ratings)
	 * 
	 * @param group
	 * 		parent to ratings Vbox
	 */
	private void ratingsVBox(VBox group) {
		VBox ratings = new VBox();

		group.getChildren().add(ratings);

		Label ratingMessage = new Label("If you would like, select and rate an item!");
		ratings.getChildren().add(ratingMessage);
		// Used later once the item ComboBox is displayed
		this.ratingMessage = ratingMessage;

		ComboBox<String> tempCB = new ComboBox<String>();

		// Additional spaces to mask the size difference of the new ComboBox containing
		// the full list
		ObservableList<String> temp = FXCollections.observableArrayList("Please load an item and rating path        ");
		tempCB = new ComboBox<String>(temp);
		ratings.getChildren().add(tempCB);
		// Used later once the item ComboBox is displayed
		this.tempCB = tempCB;

		ratingsButtons(ratings);
		Button addRating = new Button("Add Rating!");
		addRating.setOnAction(this::addRatingButtonClick);

		ratings.getChildren().add(addRating);
		// Used later once the item ComboBox is displayed
		this.addRating = addRating;

		ratings.setSpacing(10);
		this.ratingsVBox = ratings;
	}

	/**
	 * Creates comboBox of item list
	 * 
	 * @param ratingsVBox
	 * 		Vbox which will store the ComboBox
	 */
	private void itemsComboBox(VBox ratingsVBox) {
		// ratingsVBox.getChildren().clear();
		ratingsVBox.getChildren().remove(this.ratingMessage);
		ratingsVBox.getChildren().remove(this.tempCB);
		ratingsVBox.getChildren().remove(this.movieCB);
		ratingsVBox.getChildren().remove(this.bookCB);
		ratingsVBox.getChildren().remove(this.ratingsHBox);
		ratingsVBox.getChildren().remove(this.addRating);

		// If the item entered is Movie
		if (this.itemSelected) {
			ObservableList<Movie> movies = FXCollections.observableArrayList(this.movies);
			ComboBox<Movie> movieCB = new ComboBox<Movie>(movies);
			this.movieCB = movieCB;

			ratingsVBox.getChildren().add(this.ratingMessage);
			ratingsVBox.getChildren().add(movieCB);
			ratingsVBox.getChildren().add(this.ratingsHBox);
			ratingsVBox.getChildren().add(this.addRating);
		}
		// If the item entered is Book
		else {
			ObservableList<Book> books = FXCollections.observableArrayList(this.books);
			ComboBox<Book> bookCB = new ComboBox<Book>(books);
			this.bookCB = bookCB;

			ratingsVBox.getChildren().add(this.ratingMessage);
			ratingsVBox.getChildren().add(bookCB);
			ratingsVBox.getChildren().add(this.ratingsHBox);
			ratingsVBox.getChildren().add(this.addRating);
		}
	}

	/**
	 * Adds ratings to ratings' ArrayList
	 * 
	 * @param e
	 * 		when AddRatings's button is clicked
	 */
	private void addRatingButtonClick(ActionEvent e) {
		this.userId = Integer.parseInt(userIdTextField.getText());
		// If it's a list of movies
		if (this.itemSelected) {
			this.movie = this.movieCB.getValue();
			Rating rating = new Rating(this.userId + "," + this.movie.getId() + "," + this.ratingSelect);
			this.ratings.add(rating);
		}
		// If it's a list of books
		else {
			this.book = this.bookCB.getValue();
			Rating rating = new Rating(this.userId + "," + this.book.getId() + "," + this.ratingSelect);
			this.ratings.add(rating);
		}
	}

	/**
	 * Creates ratings radioButtons
	 * 
	 * @param group
	 * 		ratings section of the user's input (Vbox ratings)
	 */
	private void ratingsButtons(VBox group) {
		HBox ratingsHB = new HBox();

		// To group ratings and only make one selectable at a time
		ToggleGroup ratings = new ToggleGroup();

		// Creating the buttons
		RadioButton oneRating = new RadioButton("1");
		RadioButton twoRating = new RadioButton("2");
		RadioButton threeRating = new RadioButton("3");
		RadioButton fourRating = new RadioButton("4");
		RadioButton fiveRating = new RadioButton("5");

		// Grouping them to ratings ToggleGroup
		oneRating.setToggleGroup(ratings);
		twoRating.setToggleGroup(ratings);
		threeRating.setToggleGroup(ratings);
		fourRating.setToggleGroup(ratings);
		fiveRating.setToggleGroup(ratings);

		ratingsHB.getChildren().add(oneRating);
		ratingsHB.getChildren().add(twoRating);
		ratingsHB.getChildren().add(threeRating);
		ratingsHB.getChildren().add(fourRating);
		ratingsHB.getChildren().add(fiveRating);

		group.getChildren().add(ratingsHB);
		ratingsHB.setSpacing(5);

		oneRating.setOnAction(this::ratingSelection1);
		twoRating.setOnAction(this::ratingSelection2);
		threeRating.setOnAction(this::ratingSelection3);
		fourRating.setOnAction(this::ratingSelection4);
		fiveRating.setOnAction(this::ratingSelection5);
		this.ratingsHBox = ratingsHB;
	}

	/**
	 * Specifies that user selected the first rating
	 * 
	 * @param e
	 * 		when rating1 radioButton is selected
	 */
	private void ratingSelection1(ActionEvent e) {
		this.ratingSelect = 1;
	}
	
	/**
	 * Specifies that user selected the second rating
	 * 
	 * @param e
	 * 		when rating2 radioButton is selected
	 */
	private void ratingSelection2(ActionEvent e) {
		this.ratingSelect = 2;
	}

	/**
	 * Specifies that user selected the third rating
	 * 
	 * @param e
	 * 		when rating3 radioButton is selected
	 */
	private void ratingSelection3(ActionEvent e) {
		this.ratingSelect = 3;
	}

	/**
	 * Specifies that user selected the fourth rating
	 * 
	 * @param e
	 * 		when rating4 radioButton is selected
	 */
	private void ratingSelection4(ActionEvent e) {
		this.ratingSelect = 4;
	}

	/**
	 * Specifies that user selected the fifth rating
	 * 
	 * @param e
	 * 		when rating5 radioButton is selected
	 */
	private void ratingSelection5(ActionEvent e) {
		this.ratingSelect = 5;
	}

	/**
	 * Starts the application
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Application.launch(args);
	}

	/**
	 * Displays the recommendations
	 * 
	 * @param group
	 * 		main group
	 */
	private void resultsLayout(StackPane group) {
		VBox results = new VBox();
		// TextArea for printing results
		TextArea recommendedResults = new TextArea("\n\n\n\n\n\n\n\n\nRecommended results will appear here!");
		recommendedResults.setEditable(false);
		recommendedResults.setPrefWidth(300);
		recommendedResults.setPrefHeight(350);
		results.getChildren().add(recommendedResults);
		group.getChildren().add(results);
		results.setPadding(new Insets(75, 25, 0, 0));
		this.results = results;
		this.recommendedResults = recommendedResults;
	}
}
